module.exports = {
  // prendre en compte les modifs en live
  webpackDevMiddleware: config => {
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    }
    return config
  },

  reactStrictMode: true,
  swcMinify: false
}
